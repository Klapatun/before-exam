function debounce(func, time) {
  let iTimeout = null
  return (...args) => {
    if(iTimeout) {
      window.clearTimeout(iTimeout)
    }
    iTimeout = window.setTimeout(() => {
      func(...args)
      iTimeout = null
    }, time)
  }
}

/* Реализуйте ComboBox и экспортируйте его при помощи export {ComboBox}*/
export default class ComboBox {
  constructor(elementId, elementValue, getListOptions) {


    this.getListOptions = getListOptions;

    this.combobox = document.createElement('div');
    this.combobox.classList.add('combobox');

    const input = document.createElement('input');
    input.name = elementId;
    input.placeholder = elementValue;
    input.type = 'combobox'
    input.autocomplete = 'off';
    input.addEventListener('focus', this.showList.bind(this));
    input.addEventListener('blur', this.hideList.bind(this));
    /*INPUT*/
    input.addEventListener('input', (event)=>{
      this.hideList();
      this.showList(input.value);
      if(event.data === null) {
        this.itemIdx = 0;
      }
    });
    /*KEYUP*/
    input.addEventListener('keyup', (event)=>{
      const itemsList = this.listWrapper.getElementsByClassName('item');
      if(event.key === 'ArrowUp') {
        if(this.itemIdx > 0) {
          this.itemIdx--;
          itemsList.item(this.itemIdx+1).classList.remove('selected');
          itemsList.item(this.itemIdx).classList.add('selected');
        }
      }
      else if(event.key === 'ArrowDown') {
        if(this.itemIdx < itemsList.length-1) {
          this.itemIdx += 1;
          itemsList.item(this.itemIdx-1).classList.remove('selected');
          itemsList.item(this.itemIdx).classList.add('selected');
        }
      }
      else if(event.key === 'Enter') {
        if(!itemsList.item(this.itemIdx).classList.contains('disabled')) {
          input.value = itemsList.item(this.itemIdx).textContent;
        }
        input.blur();
      }
    });


    this.listWrapper = document.createElement('div');
    this.listWrapper.classList.add('items-wrapper');
    this.listWrapper.addEventListener('mousedown', (event)=>{
      input.value = event.target.textContent;
    });
    this.listWrapper.addEventListener('mouseover', (event)=>{
      const item = event.target;
      if(item.classList.item(0) === 'item') {
        item.classList.add('selected');
      }
    });
    this.listWrapper.addEventListener('mouseout', (event)=>{
      const item = event.target;
      if(item.classList.item(0) === 'item') {
        item.classList.remove('selected');
      }
    });


    const list = document.createElement('div');
    list.classList.add('list');
    list.appendChild(this.listWrapper);

    this.list = list;
    this.input = input;

    this.combobox.appendChild(this.input);

    return this;
  }

  showList(filter) {

    /*Для перемещения с помощью стрелочек*/
    if(!this.itemIdx) {
      this.itemIdx = 0;
    }

    this.combobox.appendChild(this.list);
    
    /*Чтобы при фокусе отправлял пустую строку, а не объект event*/
    if(filter instanceof Object) {
      filter = '';
    }

    /*Отобразить загрузку*/
    const wait = document.createElement('div');
    wait.classList.add('item');
    wait.textContent = 'Загрузка...';
    this.listWrapper.appendChild(wait);

    this.getListOptions(filter)
    .then(obj=>{
      /*Убрать загрузку*/
      wait.remove();

      if(obj.length !== 0){
        /*Вывести список*/
        obj.forEach(element => {
          const item = document.createElement('div');
          item.classList.add('item');
          item.id = element.id;
          item.textContent = element.name;

          /*Если существует введенный элемент в списке*/
          if(this.input.value === item.textContent) {
            item.classList.add('selected');
          }

          this.listWrapper.appendChild(item);
        });
      }
      else {
        const notFound = document.createElement('div');
        notFound.classList.add('item');
        notFound.classList.add('disabled');
        notFound.textContent = 'Не найдено';
        this.listWrapper.appendChild(notFound);
      }
      


      /*Выделить первый элемент*/
      if(this.itemIdx === 0) {
        this.listWrapper.getElementsByClassName('item').item(0).classList.add('selected');
      }
      
    })
    .catch(error=>{
      console.error(error);
    })
  }

  hideList() {
    while(this.listWrapper.firstChild) {
      this.listWrapper.removeChild(this.listWrapper.firstChild);
    }

    this.combobox.removeChild(this.list);
  }

  render() {
    return this.combobox;
  }
}