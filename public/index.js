/*
* В файле combobox.js реализуйте ComboBox
* а сюда импортируйте его, например так import {ComboBox} from './combobox.js'
*/

import ComboBox from './combobox.js'

/* Реализуйте функцю поиска городов */
async function getCityList(filter='') {
    if(filter) {
        filter = `?name_like=${filter}`;
    }
    try {
        const jsonObj = await fetch(`/city${filter}`);
        const arrObjs = await jsonObj.json();
        return arrObjs;
    } catch (error) {
        console.error(error);
    }
}

/* Реализуйте функцю поиска IDE */
async function getIdeList(filter='') {
    if(filter) {
        filter = `?name_like=${filter}`;
    }
    try {
        const jsonObj = await fetch(`/ide${filter}`);
        const arrObjs = await jsonObj.json();
        return arrObjs;
    }catch(error) {
        console.error(error);
    }
}

/* Добавьте ComboBox для строки "Город"
* например так:
* const city = new ComboBox('city', 'Введите или выберите из списка', getCityList)
* document.getElementById('row_city').appendChild(city.render())
* Ваша реализация может сильно отличаться
* */
const city = new ComboBox('city', 'Введите или выберите из списка', getCityList);
row_city.appendChild(city.render());

/* Добавьте ComboBox для строки "Предпочитаемая IDE"*/
const ide = new ComboBox('ide', 'IDE', getIdeList);
row_ide.appendChild(ide.render());